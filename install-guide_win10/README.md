# Requirements
1. Download newest version of Windows 10 from [Official Media](https://www.microsoft.com/en-us/software-download/windows10)
  * ISO burned to a DVD is suggested as Windows will sometimes have issues with USB installs
2. USB boot drive with an ISO of [Clonezilla](https://clonezilla.org/downloads.php)
  * _Theoretically, this can also be done by saving the image to a windows image ISO. This is not covered but might be considered._
3. Extra HDD or SSD to save Clonezilla image on _should have enough space to fit entire windows C drive_
  * If networking is available, image can be saved to: sshfs, samba, nfs, webdav, AWS S3, or Openstack Swift.

# Recommendations
* [NTFS formatted E2B USB drive](http://www.easy2boot.com/)
* [Windows10 Pro OEM key](https://www.kinguin.net/category/19429/windows-10-professional-oem-key/)
* A USB drive formatted in either NTFS or ExFat to do file transfers from a different internet connected device while the PC is to remain disconnected
  * It is also suggested to clone this git repo on to the USB.
* If installing on previously used HDD or SSD:
  * If previously a boot drive, use Clonezilla to save the image of your current install before overwriting all of your data
  * if previously a data drive, make sure to back up any data desired or it will be lost.

# Install and Prep Image
1. **Disconnect from internet**  _Or you will have to do another clean install_
2. Preform clean install of Windows 10
  * Makes sure disk which will be installed to is the first boot device in BIOS or install will fail
  * Skip the step to enter in the product key by clicking `I don't have a product key`, this will be done later after an image is made
3. Boot up Windows from the first time. **Keep disconnected from internet**
4. Disable Fast Startup
  * `Control Panel > Power Options > Choose what the power buttons do`
  * Click the `Change settings that are currently unavailable` link at the top of the window
  * Uncheck the `Turn On Fast Startup (Recommended)` option under `Shutdown Settings`
  * Click the `Save Changes` button
5. Edit local group policy (either manually **or** import from backup)
  * _If manual_:
      * Type key combo `[win]+[r]` and the type `gpedit.msc`, then `[OK]`
      * Follow this document, [windows-10-1803-local-group-policy-settings.md](https://gitlab.com/hungrykanamit/windows-tips/blob/master/install-guide_win10/install-repo/Local%20Group%20Policy/windows-10-1803-local-group-policy-settings.md)
  * _if importing from backup_: [More info on this procedure](https://community.spiceworks.com/how_to/108867-transfer-all-group-policy-settings-from-one-system-to-another)
      * Download the zipped backup file [here](https://gitlab.com/hungrykanamit/windows-tips/blob/master/install-guide_win10/install-repo/Local%20Group%20Policy/backup_06-02-2018.7z) _Download with a different internet connected device until it is safe to connect the new windows 10 install to the internet._
      * Extract the zipped backup file
      * Import the Group Policy by coping everything from the `GroupPolicy` folder in the backup into the `%windir%\System32\GroupPolicy` folder. _note that this might be a hidden folder_
      <!-- * Open dir with the extracted backup inside of `cmd (Admin)`
      * Import the backup to PC with the following: _replace `<file_path>` with the file path_
          ```
          secedit /configure /cfg <file_path>Security.csv /db defltbase.sdb /verbose
          Auditpol /restore /file:<file_path>audit.ini
          ``` -->
6. Reboot
7. Turn off UAC _makes many tasks seem faster and is not a security risk as windows is already compromised by design_
8. To remove OneDrive entirely. Download and run [RemoveOneDrive.bat](https://gitlab.com/hungrykanamit/windows-tips/blob/master/install-guide_win10/install-repo/Programs/1-required-programs/RemoveOneDrive.bat)
9. To remove Windows 10 spying features entirely. Download and open [DWS](https://github.com/Nummer/Destroy-Windows-10-Spying)
  * To run DWS, just click the button at the bottom of the window that says `Destroy Windows Spying NOW!`
  * Then allow windows to preform updates by going to `tools` and select `Enable Windows Update`
10. reboot
11. To cross check removal of Windows 10 spying features. Download and run [OOSU10](https://www.oo-software.com/en/shutup10) & make sure that at least all recommended settings are disabled
12. Reboot
13. Connect PC to internet now
14. Update Windows
15. Correct time zone if needed _most likely it is needed_
  * Right click on the clock
  * Select `Adjust date/time`
  * Set time & time zone manually
  * then set to automatic
  * verify correct time & date
16. Reboot
17. Make any Registry Key edits & install all applications desired to be on all windows images _many great and recommended programs can be found in the [install-repo](https://gitlab.com/hungrykanamit/windows-tips/tree/master/install-guide_win10/install-repo)_
* **Optional**, Change Hostname to match the pc _helpful for keeping track of images if making multiple_
  * Type key combo `[win]+[r]` and the type `sysdm.cpl`, then `[OK]`
  * Change Hostname to match the image type
  * Make any other changes desired such as: add to domain, or change paging file
* **Optional**, Install `optionalfeatures`
  * Type key combo `[win]+[r]` and the type `optionalfeatures`, then `[OK]`
  * Recommended to do the following:
    * Check `.Net Framework 3.5 (includes .Net 2.0 and 3.0)`
    * Check `Legacy Components` (with sub-options)
    * Check `Hyper-V`
    * Check `Windows Hypervisor Platform`
    * Uncheck `SMB Direct`
18. Update Windows
20. Disconnect form internet
21. Run both DWS and OOSU10 one last time after windows is fully up to date
  * To run DWS, just click the button at the bottom of the window that says `Destroy Windows Spying NOW!`
  * Then allow windows to preform updates by going to `tools` and select `Enable Windows Update`
  * Run OOSU10 and make sure that at least all recommended settings are disabled
22. Uninstall any drivers or pc specific utilities that may have installed while connected to the internet.
  * Go to `Control Panel > Programs and Features`
23. Reboot
24. Open & run `Disk Cleanup` as admin to clean system files _this makes image a lot smaller_
25. Shrink the disk as much as possible to have a smaller image size
  * Open & run `Computer Management`
  * Click `Disk Management`
  * Right Click on `C` drive
  * Select `Shrink Volume...`
26. Shut down

# Save Image
[Save Disk Image](https://clonezilla.org/show-live-doc-content.php?topic=clonezilla-live/doc/01_Save_disk_image)

# Prep first use on new PC
1. Unblock windows activation server temporarily.
  * Open DWS > Plugins > Hosts manger
  * Delete the activation server FQDN from the hostlist and select Apply
    * Unsure which is needed to be unblocked, but it should be one of the first 20 at the top of the list. Possibly one of these two:
      * `register.cdpcs.microsoft.com`
      * `support.microsoft.com`
2. Activate Windows
  * `Settings > Update & Security`
  * Click on `Change product key`
  * Enter the product key and Activate
* **Optional**, connect to a Microsoft Account _only recommended if missing product key and have a valid product key registered to your account_
  * `Settings > Accounts > Your account > Connect to a Microsoft account`
3. Run DWS again to patch the hostlist
  * In DWS > tools, select enable windows update
4. Change Hostname to match the pc
  * Type key combo `[win]+[r]` and the type `sysdm.cpl`, then `[OK]`
  * Make paging file half of the ram up to 32 GB _if 8 GB of ram, set to 4 GB paging file_
5. Install [optional programs](https://gitlab.com/hungrykanamit/windows-tips/tree/master/install-guide_win10/install-repo/Programs/3-optional-programs) if desired
* **Optional**, copy any folders named `Portable` in the `install-repo` such as the one under `1-required-programs` to `C:\`, then copy a shortcut of the `.exe` of the program to `%AppData%\Microsoft\Windows\Start Menu\Programs`, this will make the program searchable from the start menu.
6. Install any other programs you wish to install at this time, such as anti-virus, graphic drivers, etc.
