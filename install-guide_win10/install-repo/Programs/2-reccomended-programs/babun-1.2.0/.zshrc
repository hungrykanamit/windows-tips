# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="re5et"
ZSH_THEME="babun"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# History - Logging
# HISTFILE=~/.zsh_history_$(date '+%Y%m%d_%H_%M_%S_%N').txt

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

export PATH=$HOME/bin:/usr/local/bin:$PATH
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# java
#export JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.8.0_51/
#export PATH=$PATH:$EMACS_HOME/bin:$JAVA_HOME/bin
#export PATH=$PATH:$JAVA_HOME/bin

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias cd-win="cd /cygdrive/c/Users/Tom"
alias ssh-alive="ssh -o TCPKeepAlive=yes -o ServerAliveInterval=120"

logall() {
    # Log all the things
    script -af /cygdrive/c/Users/p2736134/Documents/Logs/Shell_Logs/typescript_$(date '+%F_%H-%M-%S-%N').txt
}

logprint() {
    # Make typescripts readable
    echo "Printing out typescript(s) to file."
    SHELL_LOGS=/cygdrive/c/Users/p2736134/Documents/Logs/Shell_Logs && mkdir -p $SHELL_LOGS/"originals" && cp -aur $SHELL_LOGS/typescript_* $SHELL_LOGS/originals && for i in $SHELL_LOGS/typescript_*; do perl -pe 's/\e([^\[\]]|\[.*?[a-zA-Z]|\].*?\a)//g' "$i" | col -b > "$i.log"; done && rename $SHELL_LOGS/typescript_ $SHELL_LOGS/shell-log_ $SHELL_LOGS/typescript_* && rm -r $SHELL_LOGS/shell-log_*.txt && find $SHELL_LOGS/* -name '*.txt.log' -exec sh -c 'mv "$0" "${0%.txt.log}.log"' {} \;
}

logask() {
echo "Do you want to log this shell session?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) logall; break;;
        No ) break;;
    esac
done
}

# Auto logask
# if [ $(pwd) != '/home/P2736134' ]; then
#    logask
# fi
