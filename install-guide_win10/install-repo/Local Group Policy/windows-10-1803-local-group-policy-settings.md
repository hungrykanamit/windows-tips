# Local Computer Policy > Computer Configuration > Administrative Templets > All Settings

| Policy       | Status       | Note         |
|:-------------|:-------------|:-------------|
| Allow a shared Books folder | Disable | &nbsp; |
| Allow a Windows app to share application data between users | Disable | &nbsp; |
| Allow Address bar drop-down list suggestions | Disable | &nbsp; |
| Allow Adobe Flash | Not configured | &nbsp; |
| Allow all trusted apps to install | Not configured | &nbsp; |
| Allow antimalware service to remain running always | Disable | &nbsp; |
| Allow antimalware service to startup with normal priority | Disable | &nbsp; |
| Allow auditing events in Windows Defender Application Guard | Disable | &nbsp; |
| Allow automatic update of speech Data | ‎Disable | &nbsp; |
| Allow Automatic Updates immediate installation | Disable | &nbsp; |
| Allow Cloud Search | Disable | &nbsp; |
| Allow companion device for secondary authentication | Not configured | &nbsp; |
| Allow configuration updates for the Books Library | Disable | &nbsp; |
| Allow Corporate redirection of Customer Experience Improvement uploads | Disable | &nbsp; |
| Allow Cortana | Disable | &nbsp; |
| Allow Cortana above lock screen | Disable | &nbsp; |
| Allow data persistence for Windows Defender Application Guard | Disable | &nbsp; |
| Allow deployment operations in special profiles | Disable | &nbsp; |
| Allow device name to be sent in Windows diagnostic data | Disable | &nbsp; |
| Allow downloading updates to the disk failure prediction model | Not configured | &nbsp; |
| Allow extended telemetry for the Books tab | Disable | &nbsp; |
| Allow input personalization | Disable | &nbsp; |
| Allow message service cloud sync | Disable | &nbsp; |
| Allow Microsoft accounts to be optional | Enable | &nbsp; |
| allow Microsoft services to provide enhanced suggestions as the user types in the address bar | Disable | &nbsp; |
| allow online tips | Disable | &nbsp; |
| allow publishing of user activities | Disable | &nbsp; |
| allow real-time definition updates based on reports to Microsoft maps | Disable | &nbsp; |
| allow restore of system to default state | Disable | &nbsp; |
| Allow search and Cortana to use location | Disable | &nbsp; |
| Allow suggested apps in Windows Ink Workspace | Disable | &nbsp; |
| Allow Telemetry | Disable | &nbsp; |
| Allow the use of biometrics | ‎Disable | &nbsp; |
| Allow upload of user activities | Disable | &nbsp; |
| allow Windows to automatically connect to suggested open hotspots, to networks shared by contracts, and to hotspots offering paid services | disable | &nbsp; |
| Allows development of Windows Store apps and installing them from an integrated development environment (IDE) | Not configured | &nbsp; |
| assign a default credential provider | not configured | &nbsp; |
| Automatic Maintenance Activation Boundary | Not configured | &nbsp; |
| Automatic Maintenance Random Delay | Not configured | &nbsp; |
| Automatic Maintenance WakeUp Policy | Not configured | &nbsp; |
| block all consumer Microsoft account user authentication | Not configured | &nbsp; |
| Block launching universal Windows apps with Windows Runtime API access from hosted content | Not configured | &nbsp; |
| Configure Automatic Updates | Enable | 4-Auto download and schedule the install; 1 - Every Sunday @ 5:00 AM; Install updates for other Microsoft products |
| configure the commercial id | Disable | &nbsp; |
| configure Windows defender smartscreen | disable | &nbsp; |
| continue experiences in this device | disable | &nbsp; |
| Defer Upgrades and Updates | Not configured | &nbsp; |
| Disable all apps from Windows Store | Disable | Assumed Typo |
| Disable installing Windows apps on non-system volumes | Not configured | &nbsp; |
| disable MDM enrollment | Enable | &nbsp; |
| Disable pre-release features or settings | Disable | Assumed Typo, (missing in 1803) |
| Disable Windows Error Reporting | Enable | &nbsp; |
| Do not display the lock screen | Not Configured | &nbsp; |
| do not allow video capture redirection | enable | &nbsp; |
| Do not send a Windows error report when a generic driver is installed on a device | Enable | &nbsp; |
| Do not send additional data | Enable | &nbsp; |
| Do not show feedback notifications | Enable | &nbsp; |
| Do not show Windows Tips | Enable | &nbsp; |
| Do not sync app settings | Enable | &nbsp; |
| Do not sync Apps | Enable | &nbsp; |
| Do not synchronize Windows Apps | Enable | &nbsp; |
| Don't search the web or display web results in Search | Enable | &nbsp; |
| Don't search the web or display web results in Search over metered connections | Enable | &nbsp; |
| Download Mode | Enable | Download Mode: Simple (99) |
| enable automatic MDM enrollment using default azure ad credentials | disable | &nbsp; |
| enable device health attestation monitoring and reporting | disable | &nbsp; |
| enables activity feed | Disable | &nbsp; |
| enables or disables Windows game recording and broadcasting | disable | &nbsp; |
| hide all notifications | not configured | &nbsp; |
| Hide Entry Points for Fast User Switching | Enable | &nbsp; |
| improve inking and typing recognition | disable | &nbsp; |
| Let Windows apps access account information | Enable | Select a setting: Force Deny |
| Let Windows apps access call history | Enable | Select a setting: Force Deny |
| Let windows apps access cellular data | ‎Enable | Select a setting: Force Deny |
| Let Windows apps access contacts | Enable | Select a setting: Force Deny |
| Let Windows apps access diagnostic information about other apps | ‎Enable | Select a setting: Force Deny |
| Let Windows apps access email | Enable | Select a setting: Force Deny |
| Let Windows apps access location | Enable | Select a setting: Force Deny |
| Let Windows apps access messaging | Enable | Select a setting: Force Deny |
| Let Windows apps access motion | Enable | Select a setting: Force Deny |
| Let Windows apps access notifications | Enable | Select a setting: Force Deny |
| Let Windows apps access tasks | Enable | Select a setting: Force Deny |
| Let Windows apps access the calendar | Enable | Select a setting: Force Deny |
| Let Windows apps access the camera | Enable | Select a setting: Force Deny (untested, this may block use of the camera system-wide)|
| Let Windows apps access the microphone | Not configured | This setting is system-wide. Configure from `Settings > Privacy > Microphone`|
| Let Windows apps access trusted devices | Enable | Select a setting: Force Deny |
| Let Windows apps communicate with unpaired devices | Enable | Select a setting: Force Deny |
| Let Windows apps control radios | Enable | Select a setting: Force Deny |
| Let Windows apps make phone calls | Enable | Select a setting: Force Deny |
| Let Windows apps run in the background | Enable | Select a setting: Force Deny |
| Only display the private store within the Windows Store app | Not configured | &nbsp; |
| Microsoft Customer Experience Improvement Program (CEIP) | Disable | &nbsp; |
| monitor file and program activity on your computer | Disable | &nbsp; |
| Only display the private store within the Windows Store app | Enable | &nbsp; |
| Prevent enabling lock screen camera | Enable | &nbsp; |
| Prevent enabling lock screen slide show | Enable | &nbsp; |
| prevent OneDrive file from syncing over metered connections | enable | &nbsp; |
| prevent OneDrive from gathering network traffic until the user signs in to OneDrive | enable | &nbsp; |
| Prevent the usage of OneDrive for file storage | Enable | &nbsp; |
| Prevent users' app data from being stored on non-system volumes | Enable | &nbsp; |
| prevent Windows from sending an error report when a device driver requests additional software during installation | enable | &nbsp; |
| Select an active power plan | Enable | Active Power Plan: High Performance |
| select cloud protection level | Disable | &nbsp; |
| Select the lid switch action (on battery) | Enable | Lid Switch Action: Take no Action |
| Select the lid switch action (plugged in) | Enable | Lid Switch Action: Take no Action |
| Support device authentication using certificate | Disable | &nbsp; |
| Toggle user control over Insider builds | Not configured | &nbsp; |
| Turn off access to the Store | Enable | &nbsp; |
| Turn off app notifications on the lock screen | Enable | &nbsp; |
| Turn off Application Telemetry | Enable | &nbsp; |
| Turn off Automatic Download and Install of updates | Enable | &nbsp; |
| Turn off Automatic Download and Update of Map Data | Enable | &nbsp; |
| Turn off auto-restart for updates during active hours | Not Configured | Active Hours, Start: 5 AM, End: 3 AM |
| Turn off hybrid sleep (on battery) | Enable | &nbsp; |
| Turn off hybrid sleep (plugged in) | Enable | &nbsp; |
| Turn off Microsoft consumer experiences | Enable | &nbsp; |
| turn off push to install service | enable | &nbsp; |
| Turn off System Restore | Enable | &nbsp; |
| Turn off the advertising ID | Enable | &nbsp; |
| Turn Off the hard disk (on battery) | Enable | Turn Off the Hard Disk (seconds): 0 |
| Turn Off the hard disk (plugged in) | Enable | Turn Off the Hard Disk (seconds): 0 |
| Turn off the store application | Enable | &nbsp; |
| Turn off Windows Customer Experience Improvement Program | Enable | &nbsp; |
| Turn off Windows Error Reporting | Enable | &nbsp; |
| Turn on behavior monitoring | Disable | &nbsp; |
| Turn on logging | Disable | &nbsp; |
| Use Microsoft Passport for Work | Not configured | &nbsp; |

# Local Computer Policy > User Configuration > Administrative Templets > All Settings

| Policy       | Status       | Note         |
|:-------------|:-------------|:-------------|
| allow telemetry |	Disable | &nbsp; |
| configure Windows spotlight on lock screen |	Disable | &nbsp; |
| disable logging |	enable | &nbsp; |
| disable showing balloon notifications as toasts. |	Not configured | &nbsp; |
| Disable Windows Error Reporting |	Enable | &nbsp; |
| Disk Defrangementer |	Disable | &nbsp; |
| Do not allow pinning store app to the taskbar |	Enable | &nbsp; |
| Do not send additional data |	Enable | &nbsp; |
| do not suggest third-party content in Windows spotlight |	enable | &nbsp; |
| do not use diagnostic data for tailored experiences |	enable | &nbsp; |
| Force a specific visual style file or force windows classic |	Not configured | &nbsp; |
| remove notifications and action center |	Enable | &nbsp; |
| Remove see more results / search everywhere link |	Enable | &nbsp; |
| remove the people bar from the taskbar |	enable | &nbsp; |
| Remove the search the internet 'search again' link |	Enable | &nbsp; |
| Turn off access to the Store |	Enable | &nbsp; |
| turn off all windows spotlight features |	Enable | &nbsp; |
| Turn off feature advertisement balloon notifications |	Enable | &nbsp; |
| Turn off Internet search integration |	Enable | &nbsp; |
| Turn off the store application |	Enable | &nbsp; |
| turn off the Windows welcome experience |	enable | &nbsp; |
| Turn on cloud candidate |	Disable | &nbsp; |
